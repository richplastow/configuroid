import getNearestPackagePath from './get-nearest-package-path.js'

export default function server (req, res) {
  const fs = require('fs')

  if ('GET' === req.method && '/' === req.url) {
    res.writeHead(200, { 'Content-Type': 'text/html' })
    return res.end(fs.readFileSync(__filename))
  }

  if ('GET' === req.method) {
    res.writeHead(200, { 'Content-Type': 'text/plain' })

    const nearestPackagePath = getNearestPackagePath(__dirname)
    if (! nearestPackagePath)
      return res.end(`{ "warning":"no package.json at this or higher level" }`)

    let packageJson
    try {
      packageJson = fs.readFileSync(nearestPackagePath)
    } catch (e) {
      return res.end(`{ "warning":"error reading package.json" }`)
    }

    if ('/read' === req.url)
      return res.end(`{
        "nearestPackagePath": "${nearestPackagePath}",
        "content":${packageJson}
      }`)

    res.end(`{ "warning":"GET url not supported, try '/' or '/read'" }`)
  }
}
