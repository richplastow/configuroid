export default function getNearestPackagePath (dir) {
  const fs = require('fs')
  const path = require('path')
  const candidate = path.join(dir,'package.json')
  if ( fs.existsSync(candidate) )
    return candidate
  if (! dir || '/' === dir || '\\' === dir) //@TODO is '\\' needed for Windows?
    return null
  console.log('from', dir, 'to', path.join(dir,'..'));
  return getNearestPackagePath( path.join(dir,'..') )
}
