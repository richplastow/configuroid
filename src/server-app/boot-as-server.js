import log from '../universal-lib/log.js'
import server from './server.js'

export default function bootAsServer() {
  const app = require('http').createServer(server)

  // Shut down if the server hits an error. This wouldn’t be good in production!
  app.on('error', e => {
    log('Server error: ' + e.message)
    app.close()
  })

  // Start the server.
  app.listen(3333, () => {
    log('Server is listening on port 3333')
  })
}
