import { name, version, description } from '../../package.json'

export default
`<!-- ${name} ${version} -->
<!-- --><!DOCTYPE html>
<!-- --><html lang="en">
<!-- -->  <head>
<!-- -->    <meta charset="utf-8" />
<!-- -->    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
<!-- -->    <meta name="description" content="${description}" />
<!-- -->    <link id="favicon" rel="shortcut icon" type="image/gif" href="data:image/gif;," />
<!-- -->    <title>${name} ${version}</title>
<!-- -->  </head>
<!-- -->  <body>
<!-- -->    <style>body{background:#9fc;padding-top:2em}</style>
<!-- -->    <main role="main" class="container">
<!-- -->      <h1 style="position:absolute;margin-top:-999px">${name}</h1>
<!-- -->      <img id="logo" width="352" height="32"/>
<!-- -->      <span class="h4">${version}</span>
<!-- -->      <p class="lead">${description}</p>
<!-- -->      <pre id="log"></pre>
<!-- -->      <div id="root"></div>
<!-- -->    </main>
<!-- -->    <script>
`
