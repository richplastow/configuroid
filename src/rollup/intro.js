import log from '../universal-lib/log.js'
import rebuildFn from '../universal-lib/rebuild-fn.js'
import rebuildCSS from '../universal-lib/rebuild-css.js'
import inflate from '../universal-lib/tiny-inflate-es6.min.js'
import bootstrap from '../client-lib/bootstrap.min.gz.js'
import react from '../client-lib/react.min.gz.js'
import reactDom from '../client-lib/react-dom.min.gz.js'
import getNearestPackagePath from '../server-app/get-nearest-package-path.js'
import server from '../server-app/server.js'
import bootAsServer from '../server-app/boot-as-server.js'
import bootAsClient from '../client-app/boot-as-client.js'

export default `
// Begin src/universal-lib/log.js
${log}
// End src/universal-lib/log.js

  'object'!=typeof window||document.querySelector('#logo').setAttribute('src',\`
    data:image/gif;base64,R0lGODlhsAAQAIAAAJX/y2cAzyH5BAAAAAAALAAAAACwABAAAAL/hI+py+
    0Po5y0WhWy3tvwr3ngJ44dYHJlGohOtqaxOY81eJMoC+98rvKxgKceo8dDJokhoWz3akEbyqjrYkxkrV
    TpVuuVHsVgLibM/SKq3atFPYXAr/NpHX4nr9GRvHvB9qZ3ULeXNmgYB4ioeDb2Z7YIWdY4gcfo2FaZ6C
    epmVlIiGk3GlhxKdGpWsrYCTqa+LgpyhQZ69S0+jM72UvrGepLx1r7SXkjTLrreuz5mkrMS3oxu3oom4
    xqzBltrRns3Pz83UrGfJs5Luu9Drv9234dfg4fzr28JG0ahd8/VG0uX5Iw/p4MzMZHjsCF+LDhsvEQR0
    Qd1CpavIgxo0YJAgUAADs=\`)

  'object'!=typeof window||document.querySelector('#favicon').setAttribute('href',\`
    data:image/gif;base64,R0lGODlhEAAQAIAAAJX/y2cAzyH5BAAAAAAALAAAAAAQABAAAAImhI9pwe
    2+nmRRIQqwzSFqrkDHV20JaWJi2HFoualt3E6TZzfmVgAAOw==\`)

// Begin src/universal-lib/rebuild-fn.js
${rebuildFn}
// End src/universal-lib/rebuild-fn.js

// Begin src/universal-lib/rebuild-css.js
${rebuildCSS}
// End src/universal-lib/rebuild-css.js

// Begin src/universal-lib/tiny-inflate-es6.min.js
${inflate}
// End src/universal-lib/tiny-inflate-es6.min.js

// Begin src/client-lib/bootstrap.min.gz.js
${bootstrap}
// End src/client-lib/bootstrap.min.gz.js

// Begin src/client-lib/react.min.gz.js
${react}
// End src/client-lib/react.min.gz.js

// Begin src/client-lib/react-dom.min.gz.js
${reactDom}
// End src/client-lib/react-dom.min.gz.js

// Begin src/server-app/get-nearest-package-path.js
${getNearestPackagePath}
// End src/server-app/get-nearest-package-path.js

// Begin src/server-app/server.js
${server}
// End src/server-app/server.js

// Begin src/server-app/boot-as-server.js
${bootAsServer}
// End src/server-app/boot-as-server.js

// Begin src/client-app/boot-as-client.js
${bootAsClient}
// End src/client-app/boot-as-client.js

function bootReact (response) {
  // Begin src/client-app/main.jsx
`
