import log from '../universal-lib/log.js'

export default function bootAsClient() {
  const request = new Request('http://localhost:3333/read', {
    method: 'GET'
  })
  fetch(request)
    .then(response => {
      if (response.status === 200) {
        return response.json()
      } else {
        log('Error! Something went wrong on api server!')
      }
    })
    .then(response => {
      if ( Array.isArray(response) || 'object' !== typeof response )
        return log('Error! response is not an object')
      log(JSON.stringify(response, null, '  '))
      bootReact(response)
    })
    .catch(error => {
      log('Error! (see console)')
      console.error(error)
    })
}
