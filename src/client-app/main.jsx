const { name } = response.content
ReactDOM.render(
  <div>React and ReactDOM sees initial package name <b>‘{name}’</b></div>,
  document.getElementById('root')
)
