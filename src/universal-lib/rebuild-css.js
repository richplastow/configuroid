import log from './log.js'

// Inflates a gzip string, and returns it as a function.
export default function rebuildCSS ({ log, gz, length }) {
  const rebuilt = gz
      .replace(/\s/g, '')
      .replace(/[\u0100-\u0200]/g, m => String.fromCharCode( m.charCodeAt(0)-0x100 ))
      .split('')
      .map( c => c.charCodeAt(0) )
  const cssBody = new Uint8Array(length)

  try {
    inflate(new Uint8Array(rebuilt), cssBody)
  } catch (err) {
    return log(err)
  }

  const $style = document.createElement('style')
  $style.innerText = Array.from(cssBody).map( c => String.fromCharCode(c) ).join('')
  document.head.appendChild($style)
}
