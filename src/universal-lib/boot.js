import { main } from '../../package.json'

export default
`if('object'!=typeof window)
  bootAsServer()
else if('file:'===location.protocol)
  log('Oops! can’t run under ‘file:’ protocol. Try:\\n$ node ${main}')
else bootAsClient()`
