export default function log (msg) {
  if ('object' == typeof window)
    document.querySelector('#log').innerText += '\n' + msg
  else
    console.log(msg)
}
