import log from './log.js'

// Inflates a gzip string, and returns it as a function.
export default function rebuildFn ({ log, args=[], gz, length }) {
  const rebuilt = gz
      .replace(/\s/g, '')
      .replace(/[\u0100-\u0200]/g, m => String.fromCharCode( m.charCodeAt(0)-0x100 ))
      .split('')
      .map( c => c.charCodeAt(0) )
  const fnBody = new Uint8Array(length)

  try {
    inflate(new Uint8Array(rebuilt), fnBody)
  } catch (err) {
    log(err)
    return ()=>{}
  }
  return new Function(...[ ...args,
    Array.from(fnBody).map( c => String.fromCharCode(c) ).join('')
  ])
}
