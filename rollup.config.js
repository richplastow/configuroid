import { main } from './package.json'
import banner from './src/rollup/banner.html.js'
import intro from './src/rollup/intro.js'
import outro from './src/rollup/outro.js'
import footer from './src/rollup/footer.html.js'
import babel from 'rollup-plugin-babel'
import commonjs from 'rollup-plugin-commonjs'
import resolve from 'rollup-plugin-node-resolve'

module.exports = {
  input: 'src/client-app/main.jsx',
  output: {
    file: main,
    format: 'iife',
    preferConst: true,
    banner,
    intro,
    outro,
    footer,
  },
  plugins: [
    babel({
      exclude: 'node_modules/**'
    }),
    commonjs(),
    resolve({
      // only: ['react', 'react-dom']
    })
  ]
}
